package com.plateno.im.test2;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TestIm2 {
	
	private static String url = "https://api.im.jpush.cn";
	
	public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	public static void main(String[] args) throws Exception {
		
		//testGetUserInfo();
		
		testRegister();
		
		//testRegisterAdmin();
		
		//testUpdateUserInfo();
		
		//testSendMessage();
		
		//testGetAdmins();
		
		//testGetUsers();
		
		//testChangePassword();
		
		//testDeleteUser();
		
		
		
	}
	
	public static void testDeleteUser() throws Exception {
		String changePasswordUrl = "/v1/users/%s";
		String username = "user001";
		changePasswordUrl = String.format(changePasswordUrl, username);
		
		HttpClient httpclient = new HttpClient();
		DeleteMethod deleteMethod = MethodFactory.getDeleteMethod(url + changePasswordUrl);

		int i = httpclient.executeMethod(deleteMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: " + deleteMethod.getStatusLine());    
		System.out.println("resultMessage: " + deleteMethod.getResponseBodyAsString());    
		System.out.println("test end!");
		
	}
	
	//修改密码
	public static void testChangePassword() throws Exception {
		String changePasswordUrl = "/v1/users/%s/password";
		User user = new User();
		user.setUsername("user001");
		user.setNew_password("123456");
		changePasswordUrl = String.format(changePasswordUrl, user.getUsername());
		
		HttpClient httpclient = new HttpClient();
		PutMethod putMethod = MethodFactory.getPutMethod(url + changePasswordUrl);
		RequestEntity requestEntity = new StringRequestEntity(gson.toJson(user), "application/json", "UTF-8");
		putMethod.setRequestEntity(requestEntity);

		int i = httpclient.executeMethod(putMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: " + putMethod.getStatusLine());    
		System.out.println("resultMessage: " + putMethod.getResponseBodyAsString());    
		System.out.println("test end!");
		
	}
	
	//获取用户列表
	public static void testGetUsers() throws Exception {
		String userstUrl = "/v1/users?start=%d&count=%d";
		userstUrl = String.format(userstUrl, 0, 20);
		
		HttpClient httpclient = new HttpClient();
		GetMethod getMethod = MethodFactory.getGetMethod(url + userstUrl);
		
		int i = httpclient.executeMethod(getMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: " + getMethod.getStatusLine());    
		System.out.println("resultMessage: " + getMethod.getResponseBodyAsString());    
		System.out.println("test end!");
	}
	
	//获取admin列表
	public static void testGetAdmins() throws Exception {
		String adminsUrl = "/v1/admins?start=%d&count=%d";
		adminsUrl = String.format(adminsUrl, 0, 20);

		HttpClient httpclient = new HttpClient();
		GetMethod getMethod = MethodFactory.getGetMethod(url + adminsUrl);
		
		int i = httpclient.executeMethod(getMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: " + getMethod.getStatusLine());    
		System.out.println("resultMessage: " + getMethod.getResponseBodyAsString());    
		System.out.println("test end!");
		
	}
	
	//发送消息
	public static void testSendMessage() throws Exception {
		String msgUrl = "/v1/messages";
		MsgBody msgBody = new MsgBody();
		String text = "hello, jmessage!!";
		msgBody.setText(text);
		Messages msg = new Messages();
		msg.setVersion(1);
		msg.setTarget_type("single");
		msg.setTarget_id("fcy001");
		msg.setFrom_type("admin");
		msg.setFrom_id("admin_1");
		msg.setMsg_type("text");
		msg.setMsg_body(msgBody);
		
		HttpClient httpclient = new HttpClient();
		PostMethod postMethod = MethodFactory.getPostMethod(url + msgUrl);
		RequestEntity requestEntity = new StringRequestEntity(gson.toJson(msg), "application/json", "UTF-8");
		postMethod.setRequestEntity(requestEntity);
		
		int i = httpclient.executeMethod(postMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: " + postMethod.getStatusLine());    
		System.out.println("resultMessage: " + postMethod.getResponseBodyAsString());    
		System.out.println("test end!");
		
	}
	
	//注册-管理员
	public static void testRegisterAdmin() throws Exception{
		String adminUrl = "/v1/admins";
		
		User user = new User();
		user.setUsername("admin10");
		user.setPassword("admin");
		
		HttpClient httpclient = new HttpClient();
		PostMethod postMethod = MethodFactory.getPostMethod(url + adminUrl);
		RequestEntity requestEntity = new StringRequestEntity(gson.toJson(user), "application/json", "UTF-8");
		postMethod.setRequestEntity(requestEntity);
		
		int i = httpclient.executeMethod(postMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: " + postMethod.getStatusLine());    
		System.out.println("resultMessage: " + postMethod.getResponseBodyAsString());    
		System.out.println("test end!");
		
	}
	
	//更新的userInfo
	public static void testUpdateUserInfo() throws Exception{
		String userUrl = "/v1/users/";
		String userName = "user1";
		User user = new User();
		user.setNickname("hello world3");
		
		HttpClient httpclient = new HttpClient();
		PutMethod putMethod = MethodFactory.getPutMethod(url + userUrl + userName);
		RequestEntity requestEntity = new StringRequestEntity(gson.toJson(user), "application/json", "UTF-8");
		putMethod.setRequestEntity(requestEntity);

		int i = httpclient.executeMethod(putMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: " + putMethod.getStatusLine());    
		System.out.println("resultMessage: " + putMethod.getResponseBodyAsString());    
		System.out.println("test end!");
	}
	
	//注册
	public static void testRegister() throws Exception{
		String userUrl = "/v1/users";
		User user = new User();
		user.setUsername("user001");
		user.setPassword("1234");
		List<User> list = new ArrayList<User>();
		list.add(user);
		
		HttpClient httpclient = new HttpClient();
		PostMethod postMethod = MethodFactory.getPostMethod(url + userUrl);
		RequestEntity requestEntity = new StringRequestEntity(gson.toJson(list), "application/json", "UTF-8");
		postMethod.setRequestEntity(requestEntity);
		
		int i = httpclient.executeMethod(postMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: " + postMethod.getStatusLine());    
		System.out.println("resultMessage: " + postMethod.getResponseBodyAsString());    
		System.out.println("test end!");
 	}
	
	public static void testGetUserInfo() throws Exception{
		String userUrl = "/v1/users/";
		String userName = "user1";
		HttpClient httpclient = new HttpClient();
		GetMethod getMethod = MethodFactory.getGetMethod(url + userUrl + userName);
		
		int i = httpclient.executeMethod(getMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: \n" + getMethod.getStatusLine());    
		System.out.println("resultMessage: \n" + getMethod.getResponseBodyAsString());    
		System.out.println("test end!");
	}
	
}
