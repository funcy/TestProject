package com.plateno.im.test2;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TestIm {
	
	private static String url = "https://api.im.jpush.cn";
	private static HttpHeaders headers = new HttpHeaders();
	
	public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	public static void main(String[] args) {
		headers.add("Accept", "application/json");
		headers.add("accept-charset", "utf-8");
		headers.add("Content-Type", "application/json; charset=UTF-8");
		headers.add("Authorization", "Basic ODJjZmJjODYxYjg3MDE3MmI5YzRhMDRiOjEyZGYzNWI4YmFiMmJlYWNlNDljYTI3ZQ==");
		
		//testRegister();
		
		//testRegisterAdmin();
		
		//testUpdateUserInfo();
		
		//testGetUserInfo();
		
		//testSendMessage();
		
		testGetAdmins();
		
		//testGetUsers();
		
		//testChangePassword();
		
		//testDeleteUser();
		
		
		
	}
	
	public static void testDeleteUser() {
		String changePasswordUrl = "/v1/users/%s";
		String username = "admin07";
		changePasswordUrl = String.format(changePasswordUrl, username);
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		System.out.println("entity: "+ entity.toString());
		System.out.println(entity.toString());
		
		ResponseEntity<String> resp = rest.exchange(url + changePasswordUrl, 
				HttpMethod.DELETE, entity, String.class);
		System.out.println("result: " + resp.getBody());
		System.out.println("test end!");
	}
	
	//修改密码
	public static void testChangePassword() {
		String changePasswordUrl = "/v1/users/%s/password";
		User user = new User();
		user.setUsername("user001");
		user.setNew_password("123456");
		changePasswordUrl = String.format(changePasswordUrl, user.getUsername());
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(gson.toJson(user), headers);
		System.out.println("entity: "+ entity.toString());
		System.out.println(entity.toString());
		
		ResponseEntity<String> resp = rest.exchange(url + changePasswordUrl, 
				HttpMethod.PUT, entity, String.class);
		System.out.println("result: " + resp.getBody());
		System.out.println("test end!");
		
	}
	
	//获取用户列表
	public static void testGetUsers() {
		String adminsUrl = "/v1/users?start=%d&count=%d";
		adminsUrl = String.format(adminsUrl, 1, 20);
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		System.out.println("entity: "+ entity.toString());
		
		ResponseEntity<String> resp = rest.exchange(url + adminsUrl, HttpMethod.GET, entity, String.class);
			 
		String responseText = resp.getBody();
		
		System.out.println("result: " + resp);
		System.out.println("responseText: " + responseText);
		System.out.println("test end!");
	}
	
	//获取admin列表
	public static void testGetAdmins() {
		String adminsUrl = "/v1/admins?start=%d&count=%d";
		adminsUrl = String.format(adminsUrl, 0, 20);
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		System.out.println("entity: "+ entity.toString());
		
		ResponseEntity<String> resp = rest.exchange(url + adminsUrl, HttpMethod.GET, entity, String.class);
			 
		String responseText = resp.getBody();
		
		System.out.println("result: " + resp);
		System.out.println("responseText: " + responseText);
		System.out.println("test end!");
	}
	
	//发送消息
	public static void testSendMessage() {
		String msgUrl = "/v1/messages";
		MsgBody msgBody = new MsgBody();
		String text = "hello, jmessage!!";
		msgBody.setText(text);
		Messages msg = new Messages();
		msg.setVersion(1);
		msg.setTarget_type("single");
		msg.setTarget_id("fcy001");
		msg.setFrom_type("admin");
		msg.setFrom_id("admin07");
		msg.setMsg_type("text");
		msg.setMsg_body(msgBody);
		
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(gson.toJson(msg), headers);
		System.out.println(entity.toString());
		
		String resp = rest.postForObject(url + msgUrl, entity, String.class);
		System.out.println("result: " + resp);
		System.out.println("test end!");
		
	}
	
	//获取用户信息
	public static void testGetUserInfo() {
		String userUrl = "/v1/users/";
		String userName = "admin03";
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		System.out.println("entity: "+ entity.toString());
		
		ResponseEntity<String> resp = rest.exchange(url + userUrl + userName, HttpMethod.GET, entity, String.class);
			 
		String responseText = resp.getBody();
		
		System.out.println("result: " + resp);
		System.out.println("responseText: " + responseText);
		System.out.println("test end!");
	}
	
	//注册
	public static void testRegister(){
		String userUrl = "/v1/users";
		User user = new User();
		user.setUsername("user001");
		user.setPassword("1234");
		List<User> list = new ArrayList<User>();
		list.add(user);
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(gson.toJson(list), headers);
		System.out.println(entity.toString());
		
		String resp = rest.postForObject(url + userUrl, entity, String.class);
		System.out.println("result: " + resp);
		System.out.println("test end!");
 	}
	
	//注册-管理员
	public static void testRegisterAdmin(){
		String adminUrl = "/v1/admins";
		
		User user = new User();
		user.setUsername("admin10");
		user.setPassword("admin");
		RestTemplate rest = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(gson.toJson(user), headers);
		System.out.println(entity.toString());
		
		String resp = rest.postForObject(url + adminUrl, entity, String.class);
		System.out.println("result: " + resp);
		System.out.println("test end!");
	}
	
	//更新的userInfo
	public static void testUpdateUserInfo(){
		String userUrl = "/v1/users/";
		String userName = "user1";
		RestTemplate rest = new RestTemplate();
		User user = new User();
		user.setNickname("hello world2");
		HttpEntity<String> entity = new HttpEntity<String>(gson.toJson(user), headers);
		System.out.println("entity: "+ entity.toString());
		System.out.println(entity.toString());
		
		ResponseEntity<String> resp = rest.exchange(url + userUrl + userName, 
				HttpMethod.PUT, entity, String.class);
		System.out.println("result: " + resp.getBody());
		System.out.println("test end!");
	}
	
	
}






