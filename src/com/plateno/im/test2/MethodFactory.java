package com.plateno.im.test2;

import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;

public class MethodFactory {
	
	public final static String authorizationCode = "Basic ODJjZmJjODYxYjg3MDE3MmI5YzRhMDRiOjEyZGYzNWI4YmFiMmJlYWNlNDljYTI3ZQ==";
	
	public static GetMethod getGetMethod(String url) {
		GetMethod getMethod = new GetMethod(url);
		getMethod.setRequestHeader("Accept", "application/json");
		getMethod.setRequestHeader("accept-charset", "utf-8");
		getMethod.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		getMethod.setRequestHeader("Authorization", authorizationCode);
		return getMethod;
	}
	
	public static PostMethod getPostMethod(String url) {
		PostMethod postMethod = new PostMethod(url);
		postMethod.setRequestHeader("Accept", "application/json");
		postMethod.setRequestHeader("accept-charset", "utf-8");
		postMethod.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		postMethod.setRequestHeader("Authorization", authorizationCode);
		return postMethod;
	}
	
	public static DeleteMethod getDeleteMethod(String url) {
		DeleteMethod deleteMethod = new DeleteMethod(url);
		deleteMethod.setRequestHeader("Accept", "application/json");
		deleteMethod.setRequestHeader("accept-charset", "utf-8");
		deleteMethod.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		deleteMethod.setRequestHeader("Authorization", authorizationCode);
		return deleteMethod;
	}
	
	public static PutMethod getPutMethod(String url) {
		PutMethod putMethod = new PutMethod(url);
		putMethod.setRequestHeader("Accept", "application/json");
		putMethod.setRequestHeader("accept-charset", "utf-8");
		putMethod.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		putMethod.setRequestHeader("Authorization", authorizationCode);
		return putMethod;
	}
	
}
