package com.plateno.im.test3;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

public class HttpClientUtil {

	public static CloseableHttpResponse doGet(String url, String cookie, String refer) throws IOException {
		RequestConfig config = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT).build();
		CloseableHttpClient httpClient = 
				HttpClients.custom().setDefaultRequestConfig(config).build();
		
		HttpGet httpGet = new HttpGet(url);
		if(cookie !=null ){
			httpGet.setHeader("cookie", cookie);
		}
		if(refer != null) {
			httpGet.setHeader("Referer", refer);
		}
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		return response;
		
	}
	
	public static CloseableHttpResponse doPost(String url, String dataBody, String cookie, String refer) throws IOException {
		RequestConfig config = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT).build();
		CloseableHttpClient httpClient = 
				HttpClients.custom().setDefaultRequestConfig(config).build();
		
		HttpPost httpPost = new HttpPost(url);
		if(cookie !=null ){
			httpPost.setHeader("cookie", cookie);
		}
		if(refer != null) {
			httpPost.setHeader("Referer", refer);
		}
		
		httpPost.setEntity(new StringEntity(dataBody.toString(),"UTF-8"));
		
		CloseableHttpResponse response = httpClient.execute(httpPost);
		return response;
		
	}
	
	private static X509TrustManager manager = new X509TrustManager() {
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		}

		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}
	};
	
	private static SSLConnectionSocketFactory socketFactory;
	
	private static void enableSSL() {
		try {
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new TrustManager[] { manager }, null);
			socketFactory = new SSLConnectionSocketFactory(context, NoopHostnameVerifier.INSTANCE);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} 
	}
	
	public static CloseableHttpResponse doHttpsGet(String url, String cookie, String refer) throws IOException {
		enableSSL();
		RequestConfig defaultRequestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT)
				.setExpectContinueEnabled(true).setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.DIGEST))
				.setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC)).build();
		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("http", PlainConnectionSocketFactory.INSTANCE)
				.register("https", socketFactory).build();
		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager)
				.setDefaultRequestConfig(defaultRequestConfig).build();
		
		HttpGet httpGet = new HttpGet(url);
		if(cookie !=null ){
			httpGet.setHeader("cookie", cookie);
		}
		if(refer != null) {
			httpGet.setHeader("Referer", refer);
		}
		
		CloseableHttpResponse response = httpClient.execute(httpGet);
		return response;
		
	}
	
	public static CloseableHttpResponse doHttpsGet(String url, Object object, String cookie, String refer) throws IOException {
		enableSSL();
		RequestConfig defaultRequestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT)
				.setExpectContinueEnabled(true).setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.DIGEST))
				.setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC)).build();
		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("http", PlainConnectionSocketFactory.INSTANCE)
				.register("https", socketFactory).build();
		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager)
				.setDefaultRequestConfig(defaultRequestConfig).build();
		
		HttpPost httpPost = new HttpPost(url);
		if(cookie !=null ){
			httpPost.setHeader("cookie", cookie);
		}
		if(refer != null) {
			httpPost.setHeader("Referer", refer);
		}
		httpPost.setEntity(new StringEntity(object.toString()));
		CloseableHttpResponse response = httpClient.execute(httpPost);
		return response;
		
	}
	
}











