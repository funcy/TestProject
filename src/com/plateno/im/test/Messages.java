package com.plateno.im.test;

public class Messages {
	
	private int version;
	
	private String target_type;
	
	private String from_type;
	
	private String msg_type;
	
	private String target_id;
	
	private String from_id;
	
	private MsgBody msg_body;

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTarget_type() {
		return target_type;
	}

	public void setTarget_type(String target_type) {
		this.target_type = target_type;
	}

	public String getFrom_type() {
		return from_type;
	}

	public void setFrom_type(String from_type) {
		this.from_type = from_type;
	}

	public String getMsg_type() {
		return msg_type;
	}

	public void setMsg_type(String msg_type) {
		this.msg_type = msg_type;
	}

	public String getTarget_id() {
		return target_id;
	}

	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}

	public String getFrom_id() {
		return from_id;
	}

	public void setFrom_id(String from_id) {
		this.from_id = from_id;
	}

	public MsgBody getMsg_body() {
		return msg_body;
	}

	public void setMsg_body(MsgBody msg_body) {
		this.msg_body = msg_body;
	}
	
}
