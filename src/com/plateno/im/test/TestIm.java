package com.plateno.im.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TestIm {
	
	private static String url = "https://api.im.jpush.cn";
	
	public static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
	
	public static void main(String[] args) throws Exception {
		
		//testGetUserInfo();
		
		testRegister();
		
	}
	
	//注册
		public static void testRegister() throws Exception{
			String userUrl = "/v1/users";
			User user = new User();
			user.setUsername("user001");
			user.setPassword("1234");
			List<User> list = new ArrayList<User>();
			list.add(user);
			
			HttpClient httpclient = new HttpClient();
			PostMethod postMethod = getPostMethod(url + userUrl);
			RequestEntity requestEntity = new StringRequestEntity(gson.toJson(list), "application/json", "UTF-8");
			postMethod.setRequestEntity(requestEntity);
			
			int i = httpclient.executeMethod(postMethod);
			System.out.println("运行结果：" + i);
			System.out.println("status: \n" + postMethod.getStatusLine());    
			System.out.println("resultMessage: \n" + postMethod.getResponseBodyAsString());    
			System.out.println("test end!");
	 	}
	
	public static void testGetUserInfo() throws Exception{
		String userUrl = "/v1/users/";
		String userName = "user1";
		HttpClient httpclient = new HttpClient();
		GetMethod getMethod = getGetMethod(url + userUrl + userName);
		
		int i = httpclient.executeMethod(getMethod);
		System.out.println("运行结果：" + i);
		System.out.println("status: \n" + getMethod.getStatusLine());    
		System.out.println("resultMessage: \n" + getMethod.getResponseBodyAsString());    
		System.out.println("test end!");
	}
	
	public static GetMethod getGetMethod(String url) {
		GetMethod getMethod = new GetMethod(url);
		getMethod.setRequestHeader("Accept", "application/json");
		getMethod.setRequestHeader("accept-charset", "utf-8");
		getMethod.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		getMethod.setRequestHeader("Authorization", "Basic ODJjZmJjODYxYjg3MDE3MmI5YzRhMDRiOjEyZGYzNWI4YmFiMmJlYWNlNDljYTI3ZQ==");
		return getMethod;
	}
	
	public static PostMethod getPostMethod(String url) {
		PostMethod postMethod = new PostMethod(url);
		postMethod.setRequestHeader("Accept", "application/json");
		postMethod.setRequestHeader("accept-charset", "utf-8");
		postMethod.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		postMethod.setRequestHeader("Authorization", "Basic ODJjZmJjODYxYjg3MDE3MmI5YzRhMDRiOjEyZGYzNWI4YmFiMmJlYWNlNDljYTI3ZQ==");
		return postMethod;
	}
	
	public static DeleteMethod getDeleteMethod(String url) {
		DeleteMethod deleteMethod = new DeleteMethod(url);
		deleteMethod.setRequestHeader("Accept", "application/json");
		deleteMethod.setRequestHeader("accept-charset", "utf-8");
		deleteMethod.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		deleteMethod.setRequestHeader("Authorization", "Basic ODJjZmJjODYxYjg3MDE3MmI5YzRhMDRiOjEyZGYzNWI4YmFiMmJlYWNlNDljYTI3ZQ==");
		return deleteMethod;
	}
	
	public static PutMethod getPutMethod(String url) {
		PutMethod putMethod = new PutMethod(url);
		putMethod.setRequestHeader("Accept", "application/json");
		putMethod.setRequestHeader("accept-charset", "utf-8");
		putMethod.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
		putMethod.setRequestHeader("Authorization", "Basic ODJjZmJjODYxYjg3MDE3MmI5YzRhMDRiOjEyZGYzNWI4YmFiMmJlYWNlNDljYTI3ZQ==");
		return putMethod;
	}
	
}
